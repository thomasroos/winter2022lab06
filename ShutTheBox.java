public class ShutTheBox {
	public static void main(String[] args) {
		//Welcome Message
		System.out.println("Welcome to Shut The Box!");
		// Initialize Board
		Board board = new Board();
		
		// Game Repeating
		boolean gameOver = false;
		while (!gameOver) {
			System.out.println("Player 1's Turn");
			System.out.println(board);
			
			boolean result = board.playATurn();
			if (result) {
				System.out.println("Player 2 Wins!");
				gameOver = true;
			} else {
				System.out.println("Player 2's Turn");
				System.out.println(board);
				result = board.playATurn();
				if (result) {
					System.out.println("Player 1 Wins!");
					gameOver = true;
				}
			}
		}
	}
}