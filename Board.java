public class Board {
	private Die dieOne;
	private Die dieTwo;
	private boolean[] closedTiles;
	
	public Board() {
		dieOne = new Die();
		dieTwo = new Die();
		closedTiles = new boolean[12];
	}
	
	public String toString() {
		String list = "";
		for (int i=0; i < closedTiles.length; i++) {
			if (!closedTiles[i]) {
				list = list + (i+1) + " ";
			} else {
				list = list + "X ";
			}
		}
		return list;
	}
	
	public boolean playATurn() {
		boolean returnValue = false;
		
		// Roll Dices
		dieOne.roll();
		dieTwo.roll();
		
		// Print Results
		System.out.println(dieOne);
		System.out.println(dieTwo);
		
		// Add Score
		int sum = dieOne.getPips() + dieTwo.getPips();
		
		// Checking Tiles
		if (!closedTiles[sum-1]) {
			closedTiles[sum-1] = true;
			System.out.println("Closing Tile: " + sum);
			returnValue = false;
		} else {
			System.out.println("Tile Position " + sum + " is already shut");
			returnValue = true;
		}
		return returnValue;
	}
}